package com.utfpr.delivery.service;

import com.utfpr.delivery.entity.Cliente;
import com.utfpr.delivery.entity.Usuario;
import com.utfpr.delivery.exception.BadRequestException;
import com.utfpr.delivery.exception.NotFoundException;
import com.utfpr.delivery.repository.ClienteRepository;
import com.utfpr.delivery.security.AuthenticatedUser;
import com.utfpr.delivery.utils.Utils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class ClienteService {

    @Autowired
    private AuthenticatedUser authenticatedUser;

    @Autowired
    private ClienteRepository clienteRepository;

    public List<Cliente> listar() {
        return  clienteRepository.findAll();
    }

    public Cliente getUuid(String uuid) {
        Cliente cliente = clienteRepository.findByUuid(uuid);
        if (cliente == null) {
            throw new NotFoundException("Cliente não encontrado");
        }

        return cliente;
    }

    public Cliente salvar(Cliente cliente){
        return clienteRepository.save(cliente);
    }

    public Cliente alterar(String uuid, Cliente cliente) {
        Cliente clienteAtual = this.getUuid(uuid);

        if (clienteAtual != null) {
            BeanUtils.copyProperties(cliente, clienteAtual, "id");
            return clienteRepository.save(clienteAtual);
        }

        return null;
    }

    public Cliente ajustar(String uuid, Map<String, Object> cliente) {
        Usuario usuario = authenticatedUser.getUsuario();
        Cliente clienteAtual = this.getUuid(uuid);

        if (!clienteAtual.getId().equals(usuario.getRestaurante().getId())) {
            throw new BadRequestException("Usuário não tem acesso para modificar este restaurante");
        }

        Utils.merge(clienteAtual, cliente);
        clienteAtual = this.salvar(clienteAtual);

        return clienteAtual;
    }

    public boolean excluir(String uuid) {
        Cliente cliente = this.getUuid(uuid);

        if (cliente != null) {
            try {
                clienteRepository.delete(cliente);
                return true;
            } catch (EmptyResultDataAccessException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return false;
    }

}
