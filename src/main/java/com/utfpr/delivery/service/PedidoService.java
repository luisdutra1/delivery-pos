package com.utfpr.delivery.service;

import com.utfpr.delivery.dto.pedido.PedidoInputDTO;
import com.utfpr.delivery.dto.pedidoitem.PedidoItemInputDTO;
import com.utfpr.delivery.entity.Cliente;
import com.utfpr.delivery.entity.Pedido;
import com.utfpr.delivery.entity.PedidoItem;
import com.utfpr.delivery.entity.Usuario;
import com.utfpr.delivery.exception.NotFoundException;
import com.utfpr.delivery.mapper.pedido.PedidoInputMapper;
import com.utfpr.delivery.repository.PedidoItemRepository;
import com.utfpr.delivery.repository.PedidoRepository;
import com.utfpr.delivery.security.AuthenticatedUser;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Service
public class PedidoService {

    @Autowired
    private AuthenticatedUser authenticatedUser;

    @Autowired
    private PedidoRepository pedidoRepository;

    @Autowired
    private PedidoItemRepository pedidoItemRepository;

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private ProdutoService produtoService;

    @Autowired
    private PedidoInputMapper pedidoInputMapper;


    public List<Pedido> listar() {
        return  pedidoRepository.findAll();
    }

    public Pedido getUuid(String uuid) {
//        Optional<Pedido> pedido = Optional.ofNullable(pedidoRepository.findByUuid(uuid));
//        return pedido.orElse(null);
        Pedido pedido = pedidoRepository.findByUuid(uuid);
        if (pedido == null) {
            throw new NotFoundException("Pedido não encontrado");
        }

        return pedido;
    }

    public Pedido salvar(PedidoInputDTO pedidoInputDTO){
        Pedido pedido = pedidoInputMapper.mapearEntity(pedidoInputDTO);
        pedido.setDataHora(Calendar.getInstance());

        Cliente cliente = clienteService.getUuid(pedidoInputDTO.getCliente_uuid());
        pedido.setCliente(cliente);

        Usuario usuario = authenticatedUser.getUsuario();
        pedido.setUsuario(usuario);
        pedido.setRestaurante(usuario.getRestaurante());

        pedidoRepository.save(pedido);
        pedidoRepository.flush();

        ArrayList<PedidoItem> pedidoItemList = new ArrayList<>();
        for (PedidoItemInputDTO pedidoItemInputDTO : pedidoInputDTO.getPedido_item()) {
            PedidoItem pedidoItem = new PedidoItem();
            pedidoItem.setProduto(produtoService.getUuid(pedidoItemInputDTO.getProduto_uuid()));
            pedidoItem.setQuantidade(pedidoItemInputDTO.getQuantidade());
            pedidoItem.setPedido(pedido);

            pedidoItemList.add(pedidoItem);
        }

        pedidoItemRepository.saveAll(pedidoItemList);
        pedido.setPedidoItem(pedidoItemList);

        return pedido;
    }

    public Pedido alterar(String uuid, Pedido pedido) {
        Pedido pedidoAtual = this.getUuid(uuid);

        if (pedidoAtual != null) {
            BeanUtils.copyProperties(pedido, pedidoAtual, "id");
            return pedidoRepository.save(pedidoAtual);
        }

        return null;
    }

    public boolean excluir(String uuid) {
        Pedido pedido = this.getUuid(uuid);

        if (pedido != null) {
            try {
                pedidoRepository.delete(pedido);
                return true;
            } catch (EmptyResultDataAccessException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return false;
    }

}
