package com.utfpr.delivery.service;

import com.utfpr.delivery.entity.Produto;
import com.utfpr.delivery.entity.Usuario;
import com.utfpr.delivery.exception.BadRequestException;
import com.utfpr.delivery.exception.NotFoundException;
import com.utfpr.delivery.mapper.produto.ProdutoResumoOutputMapper;
import com.utfpr.delivery.repository.ProdutoRepository;
import com.utfpr.delivery.security.AuthenticatedUser;
import com.utfpr.delivery.utils.Utils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class ProdutoService {

    @Autowired
    private AuthenticatedUser authenticatedUser;

    @Autowired
    private ProdutoRepository produtoRepository;

    @Autowired
    private ProdutoResumoOutputMapper produtoResumoOutputMapper;

    public List<Produto> listar() {
        return  produtoRepository.findAll();
    }

    public Produto getUuid(String uuid) {
        Produto produto = produtoRepository.findByUuid(uuid);
        if (produto == null) {
            throw new NotFoundException("Restaurante não encontrado");
        }

        return produto;
    }

    public Produto salvar(Produto produto){
        return produtoRepository.save(produto);
    }

    public Produto alterar(String uuid, Produto produto) {
        Produto produtoAtual = this.getUuid(uuid);

        if (produtoAtual != null) {
            BeanUtils.copyProperties(produto, produtoAtual, "id");
            return produtoRepository.save(produtoAtual);
        }

        return null;
    }

    public Produto ajustar(String uuid, Map<String, Object> produto) {
        Usuario usuario = authenticatedUser.getUsuario();
        Produto produtoAtual = this.getUuid(uuid);

        if (!produtoAtual.getId().equals(usuario.getRestaurante().getId())) {
            throw new BadRequestException("Usuário não tem acesso para modificar este restaurante");
        }

        Utils.merge(produtoAtual, produto);
        produtoAtual = this.salvar(produtoAtual);

        return produtoAtual;
    }

    public boolean excluir(String uuid) {
        Produto produto = this.getUuid(uuid);

        if (produto != null) {
            try {
                produtoRepository.delete(produto);
                return true;
            } catch (EmptyResultDataAccessException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return false;
    }
}
