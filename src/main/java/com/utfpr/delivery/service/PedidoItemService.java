package com.utfpr.delivery.service;

import com.utfpr.delivery.entity.PedidoItem;
import com.utfpr.delivery.repository.PedidoItemRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PedidoItemService {

    @Autowired
    private PedidoItemRepository pedidoItemRepository;

    public List<PedidoItem> listar() {
        return  pedidoItemRepository.findAll();
    }

    public PedidoItem getUuid(String uuid) {
        Optional<PedidoItem> PedidoItem = Optional.ofNullable(pedidoItemRepository.findByUuid(uuid));
        return PedidoItem.orElse(null);
    }

    public PedidoItem salvar(PedidoItem PedidoItem){
        return pedidoItemRepository.save(PedidoItem);
    }

    public PedidoItem alterar(String uuid, PedidoItem PedidoItem) {
        PedidoItem pedidoItemAtual = this.getUuid(uuid);

        if (pedidoItemAtual != null) {
            BeanUtils.copyProperties(PedidoItem, pedidoItemAtual, "id");
            return pedidoItemRepository.save(pedidoItemAtual);
        }

        return null;
    }

    public boolean excluir(String uuid) {
        PedidoItem PedidoItem = this.getUuid(uuid);

        if (PedidoItem != null) {
            try {
                pedidoItemRepository.delete(PedidoItem);
                return true;
            } catch (EmptyResultDataAccessException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return false;
    }

}
