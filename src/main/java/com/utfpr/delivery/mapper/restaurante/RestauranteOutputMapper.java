package com.utfpr.delivery.mapper.restaurante;

import com.utfpr.delivery.dto.restaurante.RestauranteDTO;
import com.utfpr.delivery.entity.Restaurante;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class RestauranteOutputMapper {

    @Autowired
    private ModelMapper modelMapper;

    public RestauranteDTO mapearDTO(Restaurante restaurante) {
        return modelMapper.map(restaurante, RestauranteDTO.class);
    }

    public List<RestauranteDTO> mapearLista(List<Restaurante> restaurante) {
        return restaurante.stream()
                .map(this::mapearDTO)
                .collect(Collectors.toList());
    }
}
