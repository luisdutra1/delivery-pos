package com.utfpr.delivery.mapper.restaurante;

import com.utfpr.delivery.dto.restaurante.RestauranteResumoDTO;
import com.utfpr.delivery.entity.Restaurante;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class RestauranteResumoOutputMapper {

    @Autowired
    private ModelMapper modelMapper;

    public RestauranteResumoDTO mapearDTO(Restaurante restaurante) {
        return modelMapper.map(restaurante, RestauranteResumoDTO.class);
    }

    public List<RestauranteResumoDTO> mapearLista(List<Restaurante> restaurantes) {
        return restaurantes.stream()
                .map(this::mapearDTO)
                .collect(Collectors.toList());
    }
}
