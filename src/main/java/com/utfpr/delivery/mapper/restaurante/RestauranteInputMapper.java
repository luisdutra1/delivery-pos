package com.utfpr.delivery.mapper.restaurante;

import com.utfpr.delivery.dto.restaurante.RestauranteInputDTO;
import com.utfpr.delivery.entity.Restaurante;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class RestauranteInputMapper {

	@Autowired
	private ModelMapper modelMapper;

	public Restaurante mapearEntity(RestauranteInputDTO restauranteInputDTO) {
		return modelMapper.map(restauranteInputDTO, Restaurante.class);
		
	}
	
	public List<Restaurante> mapearLista(List<RestauranteInputDTO> restauranteInputDTOs) {
		return restauranteInputDTOs.stream()
				.map(restauranteInputDTO -> mapearEntity(restauranteInputDTO))
				.collect(Collectors.toList());
		
	}
	
}
