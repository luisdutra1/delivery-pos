package com.utfpr.delivery.mapper.produto;

import com.utfpr.delivery.dto.produto.ProdutoDTO;
import com.utfpr.delivery.entity.Produto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ProdutoResumoOutputMapper {

    @Autowired
    private ModelMapper modelMapper;

    public ProdutoDTO mapearDTO(Produto produto) {
        return modelMapper.map(produto, ProdutoDTO.class);
    }

    public List<ProdutoDTO> mapearLista(List<Produto> produto) {
        return produto.stream()
                .map(this::mapearDTO)
                .collect(Collectors.toList());
    }
}
