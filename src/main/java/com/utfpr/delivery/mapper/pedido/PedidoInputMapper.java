package com.utfpr.delivery.mapper.pedido;

import com.utfpr.delivery.dto.pedido.PedidoInputDTO;
import com.utfpr.delivery.entity.Pedido;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class PedidoInputMapper {

    @Autowired
    private ModelMapper modelMapper;

    public Pedido mapearEntity(PedidoInputDTO pedidoInputDTO) {
        return modelMapper.map(pedidoInputDTO, Pedido.class);
    }

    public List<Pedido> mapearLista(List<PedidoInputDTO> pedidoInputDTOs) {
        return pedidoInputDTOs.stream()
                .map(pedidoInputDTO -> mapearEntity(pedidoInputDTO))
                .collect(Collectors.toList());
    }

}
