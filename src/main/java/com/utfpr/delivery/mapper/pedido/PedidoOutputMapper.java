package com.utfpr.delivery.mapper.pedido;

import com.utfpr.delivery.dto.pedido.PedidoResumoDTO;
import com.utfpr.delivery.entity.Pedido;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class PedidoOutputMapper {

    @Autowired
    private ModelMapper modelMapper;

    public PedidoResumoDTO mapearDTO(Pedido pedido) {
        return modelMapper.map(pedido, PedidoResumoDTO.class);
    }

    public List<PedidoResumoDTO> mapearLista(List<Pedido> pedidos) {
        return pedidos.stream()
                .map(this::mapearDTO)
                .collect(Collectors.toList());
    }
}
