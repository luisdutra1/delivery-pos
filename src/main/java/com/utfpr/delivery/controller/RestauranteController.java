package com.utfpr.delivery.controller;

import com.utfpr.delivery.dto.restaurante.RestauranteDTO;
import com.utfpr.delivery.dto.restaurante.RestauranteInputDTO;
import com.utfpr.delivery.dto.restaurante.RestauranteResumoDTO;
import com.utfpr.delivery.entity.Restaurante;
import com.utfpr.delivery.mapper.restaurante.RestauranteInputMapper;
import com.utfpr.delivery.mapper.restaurante.RestauranteOutputMapper;
import com.utfpr.delivery.mapper.restaurante.RestauranteResumoOutputMapper;
import com.utfpr.delivery.service.RestauranteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/restaurantes")
public class RestauranteController {

    @Autowired
    private RestauranteService restauranteService;

    @Autowired
    private RestauranteResumoOutputMapper restauranteResumoOutputMapper;

    @Autowired
    private RestauranteOutputMapper restauranteOutputMapper;

    @Autowired
    private RestauranteInputMapper restauranteInputMapper;

    @GetMapping
    @ResponseBody
    public List<RestauranteResumoDTO> listar() {
        return restauranteResumoOutputMapper.mapearLista(
                restauranteService.listar()
        );
    }

    @GetMapping("/{uuid}")
    @ResponseBody
    public RestauranteDTO getRestauranteByUuid(@PathVariable String uuid) {

        Restaurante restaurante = restauranteService.getUuid(uuid);

        RestauranteDTO restauranteDTO = restauranteOutputMapper.mapearDTO(restaurante);

        return restauranteDTO;

    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    private RestauranteDTO adicionar(@RequestBody @Valid RestauranteInputDTO restauranteInputDTO) {

        Restaurante restaurante = restauranteInputMapper.mapearEntity(restauranteInputDTO);

        restaurante = restauranteService.salvar(restaurante);

        RestauranteDTO restauranteDTO = restauranteOutputMapper.mapearDTO(restaurante);

        return restauranteDTO;

    }

    @PutMapping("/{uuid}")
    @ResponseBody
    private RestauranteDTO alterar(@PathVariable String uuid, @Valid @RequestBody RestauranteInputDTO restauranteInputDTO) {

        Restaurante restaurante = restauranteInputMapper.mapearEntity(restauranteInputDTO);

        restaurante = restauranteService.alterar(uuid, restaurante);

        RestauranteDTO restauranteDTO = restauranteOutputMapper.mapearDTO(restaurante);

        return restauranteDTO;

    }

    @PatchMapping("/{uuid}")
    @ResponseBody
    private Restaurante ajustar(@PathVariable String uuid, @RequestBody RestauranteInputDTO restauranteInputDTO) {

        Restaurante restaurante = restauranteInputMapper.mapearEntity(restauranteInputDTO);

        return restauranteService.alterar(uuid, restaurante);

    }

    @DeleteMapping("/{uuid}")
    private ResponseEntity<Restaurante> deletar(@PathVariable String uuid) {

        if (restauranteService.excluir(uuid)) {

            return ResponseEntity.noContent().build();

        } else {

            return ResponseEntity.badRequest().build();

        }

    }

}

