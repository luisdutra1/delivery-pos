package com.utfpr.delivery.controller;

import com.utfpr.delivery.dto.cliente.ClienteDTO;
import com.utfpr.delivery.dto.cliente.ClienteInputDTO;
import com.utfpr.delivery.entity.Cliente;
import com.utfpr.delivery.mapper.cliente.ClienteInputMapper;
import com.utfpr.delivery.mapper.cliente.ClienteOutputMapper;
import com.utfpr.delivery.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/clientes")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private ClienteOutputMapper clienteOutputMapper;

    @Autowired
    private ClienteInputMapper clienteInputMapper;

    @GetMapping
    @ResponseBody
    public List<ClienteDTO> listar() {
        List<Cliente> cliente = clienteService.listar();
        return clienteOutputMapper.mapearLista(cliente);
    }

    @GetMapping("/{uuid}")
    @ResponseBody
    public Cliente getUuid(@PathVariable String uuid) {
        return clienteService.getUuid(uuid);
    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    private ClienteDTO adicionar(@RequestBody @Valid ClienteInputDTO clienteInputDTO) {

        Cliente cliente = clienteInputMapper.mapearEntity(clienteInputDTO);
        cliente = clienteService.salvar(cliente);

        return clienteOutputMapper.mapearDTO(cliente);
    }

    @PutMapping("/{id}")
    public ClienteDTO alterar(@PathVariable String uuid, @Valid @RequestBody ClienteInputDTO clienteInputDTO) {
        Cliente cliente = clienteInputMapper.mapearEntity(clienteInputDTO);
        cliente = clienteService.alterar(uuid, cliente);
        return clienteOutputMapper.mapearDTO(cliente);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deletar(@PathVariable String uuid) {
        if (clienteService.excluir(uuid)) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.badRequest().build();
        }
    }
}
