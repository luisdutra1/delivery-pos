package com.utfpr.delivery.controller;

import com.utfpr.delivery.dto.produto.ProdutoDTO;
import com.utfpr.delivery.dto.produto.ProdutoInputDTO;
import com.utfpr.delivery.entity.Produto;
import com.utfpr.delivery.entity.Restaurante;
import com.utfpr.delivery.mapper.produto.ProdutoResumoInputMapper;
import com.utfpr.delivery.mapper.produto.ProdutoResumoOutputMapper;
import com.utfpr.delivery.service.ProdutoService;
import com.utfpr.delivery.service.RestauranteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/produtos")
public class ProdutoController {

    @Autowired
    private ProdutoService produtoService;

    @Autowired
    private RestauranteService restauranteService;

    @Autowired
    private ProdutoResumoOutputMapper produtoResumoOutputMapper;

    @Autowired
    private ProdutoResumoInputMapper produtoResumoInputMapper;

    @GetMapping
    @ResponseBody
    public List<ProdutoDTO> listar() {
        List<Produto> produtos = produtoService.listar();
        return produtoResumoOutputMapper.mapearLista(produtos);
    }

    @GetMapping("/{uuid}")
    @ResponseBody
    public Produto getId(@PathVariable String uuid) {
        return produtoService.getUuid(uuid);
    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    private ProdutoDTO adicionar(@RequestBody @Valid ProdutoInputDTO produtoInputDTO) {

        Produto produto = produtoResumoInputMapper.mapearEntity(produtoInputDTO);
        Restaurante restaurante = restauranteService.getUuid(produtoInputDTO.getRestaurante_uuid());

        produto.setRestaurante(restaurante);
        produto = produtoService.salvar(produto);

        return  produtoResumoOutputMapper.mapearDTO(produto);
    }

    @PutMapping("/{id}")
    public Produto alterar(@PathVariable String uuid, @RequestBody Produto produto) {
        return produtoService.alterar(uuid, produto);
    }

    @PatchMapping("/{id}")
    public Produto ajustar(@PathVariable String uuid, @RequestBody Map<String, Object> produto) {
        return produtoService.ajustar(uuid, produto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deletar(@PathVariable String uuid) {
        if (produtoService.excluir(uuid)) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.badRequest().build();
        }
    }
}
