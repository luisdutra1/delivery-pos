package com.utfpr.delivery.controller;

import com.utfpr.delivery.dto.pedido.PedidoInputDTO;
import com.utfpr.delivery.dto.pedido.PedidoResumoDTO;
import com.utfpr.delivery.entity.Pedido;
import com.utfpr.delivery.mapper.pedido.PedidoInputMapper;
import com.utfpr.delivery.mapper.pedido.PedidoOutputMapper;
import com.utfpr.delivery.service.PedidoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/pedidos")
public class PedidoController {

    @Autowired
    private PedidoService pedidoService;

    @Autowired
    private PedidoOutputMapper pedidoOutputMapper;

    @Autowired
    private PedidoInputMapper pedidoInputMapper;

    @GetMapping
    @ResponseBody
    public List<PedidoResumoDTO> listar() {
        List<Pedido> pedido = pedidoService.listar();
        return pedidoOutputMapper.mapearLista(pedido);
    }

    @GetMapping("/{id}")
    @ResponseBody
    public Pedido getUuid(@PathVariable String uuid) {
        return pedidoService.getUuid(uuid);
    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    private PedidoResumoDTO adicionar(@RequestBody @Valid PedidoInputDTO pedidoInputDTO) {
        return pedidoOutputMapper.mapearDTO(pedidoService.salvar(pedidoInputDTO));
    }

    @PutMapping("/{id}")
    public PedidoResumoDTO alterar(@PathVariable String uuid, @Valid @RequestBody PedidoInputDTO pedidoInputDTO) {
        Pedido pedido = pedidoInputMapper.mapearEntity(pedidoInputDTO);
        pedido = pedidoService.alterar(uuid, pedido);
        return pedidoOutputMapper.mapearDTO(pedido);
    }
    
    @DeleteMapping("/{uuid}")
    public ResponseEntity deletar(@PathVariable String uuid) {
        if (pedidoService.excluir(uuid)) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.badRequest().build();
        }
    }
}
