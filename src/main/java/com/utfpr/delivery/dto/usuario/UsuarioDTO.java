package com.utfpr.delivery.dto.usuario;

import lombok.Data;

@Data
public class UsuarioDTO {

    private String nome;
}
