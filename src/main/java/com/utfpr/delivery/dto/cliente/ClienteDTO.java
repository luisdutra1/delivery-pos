package com.utfpr.delivery.dto.cliente;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ClienteDTO {

    private String uuid;
    private String nome;
    private String email;
    private String telefone;
    private BigDecimal limiteCredito;

}
