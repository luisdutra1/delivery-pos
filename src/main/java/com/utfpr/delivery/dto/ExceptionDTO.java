package com.utfpr.delivery.dto;

import lombok.Data;

import java.util.List;

@Data
public class ExceptionDTO {

	private Integer status;
	
	private String mensagem;
	
	private List<Validacao> validacoes;
	
	@Data
	public static class Validacao {
		
		private String campo;
		
		private String erro;
		
	}
	
}
