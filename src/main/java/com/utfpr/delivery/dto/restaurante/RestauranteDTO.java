package com.utfpr.delivery.dto.restaurante;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class RestauranteDTO {

    private String uuid;
    private String nome;
    private BigDecimal taxaFrete;
}
