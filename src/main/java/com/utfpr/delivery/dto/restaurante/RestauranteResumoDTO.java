package com.utfpr.delivery.dto.restaurante;

import lombok.Data;

@Data
public class RestauranteResumoDTO {

    private String uuid;
    private String nome;

}
