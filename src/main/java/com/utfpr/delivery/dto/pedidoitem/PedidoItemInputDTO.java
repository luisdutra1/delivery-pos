package com.utfpr.delivery.dto.pedidoitem;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class PedidoItemInputDTO {

    @NotBlank
    private String produto_uuid;

    @NotBlank
    private Integer quantidade;
}
