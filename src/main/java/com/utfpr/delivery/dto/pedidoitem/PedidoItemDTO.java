package com.utfpr.delivery.dto.pedidoitem;

import com.utfpr.delivery.dto.produto.ProdutoResumoDTO;
import lombok.Data;

@Data
public class PedidoItemDTO {

    private ProdutoResumoDTO produto;
    private Integer quantidade;

}
