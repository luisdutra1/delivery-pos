package com.utfpr.delivery.dto.pedido;

import com.utfpr.delivery.dto.pedidoitem.PedidoItemInputDTO;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
public class PedidoInputDTO {

    @NotBlank
    private String cliente_uuid;

    private List<PedidoItemInputDTO> pedido_item;
}
