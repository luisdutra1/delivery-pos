package com.utfpr.delivery.dto.pedido;

import com.utfpr.delivery.dto.cliente.ClienteDTO;
import com.utfpr.delivery.dto.pedidoitem.PedidoItemDTO;
import com.utfpr.delivery.dto.restaurante.RestauranteResumoDTO;
import com.utfpr.delivery.dto.usuario.UsuarioDTO;
import lombok.Data;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

@Data
public class PedidoResumoDTO {

    private Calendar dataHora;
    private RestauranteResumoDTO restaurante;
    private ClienteDTO cliente;
    private UsuarioDTO usuario;
    private List<PedidoItemDTO> pedidoItem;

    public String getDataHora() {
        SimpleDateFormat sdfData = new SimpleDateFormat("dd/MM/yyyy");
        String dataFormatada = sdfData.format(dataHora.getTime());

        return dataFormatada;
    }
}
