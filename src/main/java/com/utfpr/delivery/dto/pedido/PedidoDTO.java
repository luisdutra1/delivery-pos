package com.utfpr.delivery.dto.pedido;

import com.utfpr.delivery.dto.cliente.ClienteDTO;
import com.utfpr.delivery.dto.pedidoitem.PedidoItemDTO;
import com.utfpr.delivery.dto.restaurante.RestauranteDTO;
import com.utfpr.delivery.dto.usuario.UsuarioDTO;
import lombok.Data;

import java.util.List;

@Data
public class PedidoDTO {

    private String uuid;
    private RestauranteDTO restaurante;
    private ClienteDTO cliente;
    private UsuarioDTO usuario;
    private List<PedidoItemDTO> pedidoItem;

}
