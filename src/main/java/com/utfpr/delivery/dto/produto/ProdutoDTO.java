package com.utfpr.delivery.dto.produto;

import com.utfpr.delivery.dto.restaurante.RestauranteDTO;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class ProdutoDTO {

    private String uuid;
    private String nome;
    private BigDecimal preco;
    private RestauranteDTO restaurante;

}
