package com.utfpr.delivery.dto.produto;

import lombok.Data;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class ProdutoInputDTO {

    @NotBlank
    private String nome;

    @NotNull
    @DecimalMin(value = "0.00", inclusive = false)
    @Digits(integer = 15, fraction = 2)
    private BigDecimal preco;

    @NotBlank
    private String restaurante_uuid;
}
