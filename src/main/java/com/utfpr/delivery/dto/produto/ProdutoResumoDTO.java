package com.utfpr.delivery.dto.produto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ProdutoResumoDTO {

    private String nome;
    private BigDecimal preco;

}
