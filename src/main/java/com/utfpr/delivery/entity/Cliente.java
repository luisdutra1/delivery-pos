package com.utfpr.delivery.entity;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.UUID;

@Data
@Entity
@Table(name = "cliente")
public class Cliente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "uuid", length = 36)
    private String uuid;

    @Column(length = 100, nullable = false)
    private String nome;

    @Column(length = 150)
    private String email;

    @Column(length = 11)
    private String telefone;

    @Column(name = "limite_credito", nullable = false)
    private BigDecimal limiteCredito;

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "resturante_id", nullable = false)
//    private Restaurante restaurante;

//    Se precisar manter a lista de clientes no restuarante descomentar essas linhas
//    @Column(name="restaurante_id", nullable = false)
//    private Long restaurante;

    @PrePersist
    private void gerarUUID() {
        setUuid(UUID.randomUUID().toString());
    }
}
