package com.utfpr.delivery.entity;

import lombok.Data;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

@Data
@Entity
@Table(name = "pedido")
public class Pedido {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "uuid", length = 36)
    private String uuid;

    @Column(name = "data_hora")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar dataHora;

    @OneToOne
    @JoinColumn(name = "restaurante_id")
    private Restaurante restaurante;

    @OneToOne
    @JoinColumn(name = "cliente_id")
    private Cliente cliente;

    @OneToOne
    @JoinColumn(name = "usuario_id")
    private Usuario usuario;

    @OneToMany(mappedBy = "pedido")
    @Cascade(org.hibernate.annotations.CascadeType.DELETE)
    private List<PedidoItem> pedidoItem;

    @PrePersist
    private void gerarUUID() {
        setUuid(UUID.randomUUID().toString());
    }
}
