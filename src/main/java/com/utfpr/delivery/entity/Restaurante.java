package com.utfpr.delivery.entity;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.UUID;

@Data
@Entity
@Table(name = "restaurante")
public class Restaurante {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "uuid", length = 36)
    private String uuid;

    private String nome;

    @Column(name = "taxa_frete")
    private BigDecimal taxaFrete;

//    @OneToMany(mappedBy = "restaurante")
//    private List<Cliente> clientes;

    @PrePersist
    private void gerarUUID() {
        setUuid(UUID.randomUUID().toString());
    }
}
