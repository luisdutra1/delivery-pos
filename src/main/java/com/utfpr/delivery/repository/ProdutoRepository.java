package com.utfpr.delivery.repository;

import com.utfpr.delivery.entity.Produto;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProdutoRepository extends JpaRepository<Produto, Long> {

    Produto findByUuid(String uuid);
}
