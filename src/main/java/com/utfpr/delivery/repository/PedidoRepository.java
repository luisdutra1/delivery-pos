package com.utfpr.delivery.repository;

import com.utfpr.delivery.entity.Pedido;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PedidoRepository extends JpaRepository<Pedido, Long> {

    Pedido findByUuid(String uuid);
}
