package com.utfpr.delivery.repository;

import com.utfpr.delivery.entity.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClienteRepository extends JpaRepository<Cliente, Long> {

    Cliente findByUuid(String uuid);
}
