CREATE TABLE restaurante (
	id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    uuid VARCHAR(36) NOT NULL,
    nome VARCHAR(255) NOT NULL,
    taxa_frete DECIMAL(15,2) NOT NULL
);

CREATE TABLE usuario (
	id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    uuid VARCHAR(36) NOT NULL,
    nome VARCHAR(255) NOT NULL,
    username VARCHAR(100) NOT NULL,
    email VARCHAR(100) NOT NULL,
    password VARCHAR(100) NOT NULL,
    status TINYINT NOT NULL,
    restaurante_id INT NOT NULL,
    CONSTRAINT `fk_usuario_restaurante`
        FOREIGN KEY (`restaurante_id`)
        REFERENCES `restaurante` (`id`)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION
);

INSERT INTO `delivery`.`restaurante` (`id`, `nome`, `taxa_frete`, `uuid`) VALUES ('1', 'Restaurante do Adriano', '10', '00b8fd02-eba0-48ed-8806-8d88176f9c42');

INSERT INTO `delivery`.`usuario` (`id`, `email`, `nome`, `password`, `status`, `username`, `uuid`, `restaurante_id`) VALUES ('1', 'luis@gmail.com', 'Luis', '$2a$12$t/3.Lj4dxAL6dn1ShcfeBewx7RMjZzfbiLGP8Fji3FUj3V4wqwa5C', 1, 'luis', '7e9779f6-15da-4457-b7ee-e026329cb605', 1);

CREATE TABLE cliente (
     id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
     uuid VARCHAR(36) NOT NULL,
     nome VARCHAR(100) NOT NULL,
     email VARCHAR(100) NOT NULL,
     telefone VARCHAR(11) NOT NULL,
     limite_credito DOUBLE NOT NULL
);

CREATE TABLE produto (
     id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
     restaurante_id INT NOT NULL,
     uuid VARCHAR(36) NOT NULL,
     nome VARCHAR(255) NOT NULL,
     preco DOUBLE NOT NULL,
     CONSTRAINT `fk_produto_restaurante`
         FOREIGN KEY (`restaurante_id`)
             REFERENCES `restaurante` (`id`)
             ON DELETE NO ACTION
             ON UPDATE NO ACTION
);

CREATE TABLE pedido (
     id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
     data_hora TIMESTAMP NOT NULL,
     usuario_id INT NOT NULL,
     restaurante_id INT NOT NULL,
     cliente_id INT NOT NULL,
     uuid VARCHAR(36) NOT NULL,
     CONSTRAINT `fk_pedido_restaurante`
         FOREIGN KEY (`restaurante_id`)
             REFERENCES `restaurante` (`id`)
             ON DELETE NO ACTION
             ON UPDATE NO ACTION,
     CONSTRAINT `fk_pedido_cliente`
         FOREIGN KEY (`cliente_id`)
             REFERENCES `cliente` (`id`)
             ON DELETE NO ACTION
             ON UPDATE NO ACTION,
     CONSTRAINT `fk_pedido_usuario`
         FOREIGN KEY (`usuario_id`)
             REFERENCES `usuario` (`id`)
             ON DELETE NO ACTION
             ON UPDATE NO ACTION
);


CREATE TABLE pedido_item (
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    pedido_id INT NOT NULL,
    produto_id INT NOT NULL,
    uuid VARCHAR(36) NOT NULL,
    quantidade INT NOT NULL,
    CONSTRAINT `fk_pedido_item_pedido`
        FOREIGN KEY (`pedido_id`)
            REFERENCES `pedido` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
    CONSTRAINT `fk_pedido_item_produto`
        FOREIGN KEY (`produto_id`)
            REFERENCES `produto` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
);